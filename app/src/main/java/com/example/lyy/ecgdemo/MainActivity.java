package com.example.lyy.ecgdemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MainActivity extends Activity {

    private ECGSurfaceView mView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        mView = (ECGSurfaceView) findViewById(R.id.surfaceView);
        mView.lineNumber = 1;
        mView.secondNumber = 10;
        mView.amplify = 1;
        Spinner line_spinner = (Spinner) findViewById(R.id.line_spinner);
        String[] lineItems = getResources().getStringArray(R.array.line_array);
        final ArrayAdapter line_adapter = new ArrayAdapter<>(this, R.layout.my_spinner, lineItems);
        line_adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        line_spinner.setAdapter(line_adapter);

        Spinner second_spinner = (Spinner) findViewById(R.id.second_spinner);
        String[] secondItems = getResources().getStringArray(R.array.second_array);
        final ArrayAdapter second_adapter = new ArrayAdapter<>(this, R.layout.my_spinner, secondItems);
        second_adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        second_spinner.setAdapter(second_adapter);
        Spinner amplify_spinner = (Spinner) findViewById(R.id.amplify_spinner);
        String[] amplifyItems = getResources().getStringArray(R.array.amplify_array);
        final ArrayAdapter amplify_adapter = new ArrayAdapter<>(this, R.layout.my_spinner, amplifyItems);
        amplify_adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        amplify_spinner.setAdapter(amplify_adapter);


        amplify_spinner.setSelection(4,true);

        line_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mView.lineNumber = Integer.parseInt(line_adapter.getItem(position).toString());
                mView.myThread.run();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        second_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mView.secondNumber = Integer.parseInt(second_adapter.getItem(position).toString());
                mView.myThread.run();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        amplify_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mView.amplify = Float.parseFloat(amplify_adapter.getItem(position).toString());
                mView.myThread.run();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}

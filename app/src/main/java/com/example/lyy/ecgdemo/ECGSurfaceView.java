package com.example.lyy.ecgdemo;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.storage.StorageManager;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

/**
 * Created by lyy on 2015/8/18.
 */
public class ECGSurfaceView extends SurfaceView implements SurfaceHolder.Callback {


    public int lineNumber, secondNumber;
    public float amplify;
    private Context context;
    private String filePath;

    /**
     * 画笔对象
     */
    private Paint linePaint = new Paint();

    /**
     * 心电数据集合
     */
    private ArrayList<Short> dataListY = new ArrayList<>();
    /**
     * 在画布上正在显示的数据集合
     */

    public MyThread myThread;

    public ECGSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        this.context = context;
        myThread = new MyThread(holder);
        getPath();
        getData();

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        //myThread.isRun = true;
        myThread.start();
        //System.out.println("surfaceCreated");
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        //System.out.println("surfaceChanged");
    }



    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        //myThread.isRun = false;

        //System.out.println("surfaceDestroyed");
    }

    public void getData() {
        File file = new File(filePath);

        try {
            FileInputStream fis = new FileInputStream(file);
            DataInputStream dInputStream = new DataInputStream(fis);
            try {

                for (int i = 0; i < 256 * 360; i++) {
                    int ch2 = dInputStream.read();
                    int ch1 = dInputStream.read();
                    short data = (short) ((ch1 << 8) + (ch2));
                    dataListY.add(data);
                    //System.out.println("==== " + (short)((ch1 << 8) + (ch2)));
                }

            } finally {
                dInputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 画图的方法
     */
    private Canvas doDraw(Canvas canvas) {
        super.draw(canvas);
        /**
         * 显示的波形每个单元格开始的位置(X轴坐标)
         */
        float showedBeginX;

        /**
         * 显示的波形每个单元格结束的位置(X轴坐标)
         */
        float showedEndX;

        /**
         * 显示的波形每个单元格开始的时候波形长度的值(Y轴坐标)
         */
        float showedBeginY;

        /**
         * 显示的波形每个单元格结束的时候波形长度的值(Y轴坐标)
         */
        float showedEndY;


        int width = canvas.getWidth();
        int height = canvas.getHeight();


            /*
      已经显示的数据波形 最多能够显示的单位格数量(横坐标 15px 为一个单位格)
     */


        int maxNum = 256 * secondNumber;
        double scale = (double) width / (double) maxNum;
        //int maxNum = (int) ((width - scale) / scale);
        //System.out.println("== " + scale + "==" + width + "==" + maxNum);


        // 画背景格子
        int lStartX = 0;
        int lStartY = 0;
        int temp = 0;
        for (int i = 0; i < height; i++) {
            linePaint.setStrokeWidth(1);
            linePaint.setColor(Color.GRAY);
            canvas.drawLine(lStartX, lStartY, width, lStartY, linePaint);
//			canvas.drawLine(lStartX, lStartY, height/7+1, lStartY, linePaint);
            lStartY += 15;
            canvas.drawLine(temp, 0, temp, height, linePaint);

            //控制竖线条数(最大位置不能超过width)
            if (temp < width) {
                temp += 15;
            }
        }


        for (int i = 0; i < lineNumber; i++) {
            linePaint.setColor(Color.GREEN);
            linePaint.setStrokeWidth(2);
            showedBeginX = 0;
            showedEndX = (float) (showedBeginX + scale);
            showedBeginY = (i + 1) * height / (lineNumber + 1) - dataListY.get(i * maxNum) * amplify;
            for (int j = i * maxNum; j < maxNum * (i + 1); j++) {
                showedEndY = (i + 1) * height / (lineNumber + 1) - dataListY.get(j) * amplify;
                canvas.drawLine(showedBeginX, showedBeginY, showedEndX, showedEndY, linePaint);
                showedBeginX = showedEndX;
                showedEndX = (float) (showedBeginX + scale);
                showedBeginY = showedEndY;
            }
        }

        return canvas;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void getPath() {
        StorageManager sm = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
        File file;
        String originFilePath;

        // 获取sdcard的路径：外置和内置
        try {
            String[] paths = (String[]) sm.getClass().getMethod("getVolumePaths", null).invoke(sm, null);
            for (String temp : paths) {

                //System.out.println("路径：" + filePath);
                originFilePath = temp + context.getResources().getString(R.string.path);
                file = new File(originFilePath);

                if (file.exists()) {
                    filePath = originFilePath;
                    break;
                }

            }

        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
           // e.printStackTrace();
        }

    }

    public class MyThread extends Thread {

        public SurfaceHolder holder;
        //public boolean isRun = true;

        public MyThread(SurfaceHolder holder) {
            this.holder = holder;

        }

        @Override
        public void run() {

            Canvas mCanvas = null;
            //while (isRun) {
                try {

                    mCanvas = holder.lockCanvas();
                    if (null != mCanvas) {
                        mCanvas.drawColor(Color.BLACK);

                        doDraw(mCanvas);
                        Thread.sleep(100);
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if (mCanvas != null) {

                        holder.unlockCanvasAndPost(mCanvas);
                    }
                }
            //}


        }
    }
}
